#include "utils.h"

bool hrv::utils::IsPowerOfTwo(unsigned long long x)
{
    return (x != 0) && ((x & (x - 1)) == 0);
}
