#pragma once

#include <algorithm>
#include <iterator>
#include <vector>
#include <future>
#include <logger.hpp>
#include "utils.h"

namespace hrv
{

    template <typename T>
    class Polynomial
    {
    private:
        using value_type = typename std::decay<T>::type;

        void FillEndUntilSizePowOfTwo(std::vector<T>& vec);
        void FillUntilSizeEqualTo(size_t size);
        void FillBeginUntilSizeEqualTo(std::vector<T>& vec, size_t size);
        void FillEndUntilSizeEqualTo(std::vector<T>& vec, size_t size);
        std::vector<T> GetLeftSide(std::vector<T>& vec);
        std::vector<T> GetRightSide(std::vector<T>& vec);

        std::vector<T> AddVectors(std::vector<T> v1, std::vector<T> v2);
        std::vector<T> SubtractVectors(std::vector<T> v1, std::vector<T> v2);

        template<typename U>
        friend size_t GetDegree(const Polynomial<U>& poly);
    public:

        Polynomial() {}
        Polynomial(size_t degree);

        hrv::Polynomial<T>& SetFillValue(const T& val);
        std::vector<value_type> GetCoefs() const { return _coefs; }

        template<class InputIterator>
        hrv::Polynomial<T>& SetCoefs(InputIterator first, InputIterator last);
        hrv::Polynomial<T>& SetCoefAt(const size_t& index, const T& val);
        T& GetCoefAt(const size_t& index);

        template<typename U>
        friend hrv::Polynomial<U> operator+(Polynomial<U> lhs, const Polynomial<U>& rhs);
        template<typename U>
        friend hrv::Polynomial<U> operator-(Polynomial<U> lhs, const Polynomial<U>& rhs);
        template<typename U>
        friend hrv::Polynomial<U> operator*(const int& val, const Polynomial<U>& rhs);

        hrv::Polynomial<T> operator*(const T& val) const;

        hrv::Polynomial<T> MultiplyRegularSingleThreaded(const Polynomial<T>& ot);
        hrv::Polynomial<T> MultiplyRegularMultiThreaded(const Polynomial<T>& ot, const size_t& thread_count);
        std::vector<T> MultiplyKaratsubaVectors(std::vector<T>& p1, std::vector<T>& p2, bool use_threads);
        hrv::Polynomial<T> MultiplyKaratsubaSingleThreaded(Polynomial<T>& ot);
        hrv::Polynomial<T> MultiplyKaratsubaMultiThreaded(Polynomial<T>& ot);

        ~Polynomial();
    private:
        T _fill_val;
        std::vector<value_type> _coefs;
    };

    template<typename T>
    inline std::vector<T> Polynomial<T>::MultiplyKaratsubaVectors(std::vector<T> & p1, std::vector<T> & p2, bool use_threads)
    {
        std::vector<T> p1_coefs = p1;
        std::vector<T> p2_coefs = p2;
        FillEndUntilSizePowOfTwo(p1_coefs);
        FillEndUntilSizePowOfTwo(p2_coefs);

        if (p1_coefs.size() == p2_coefs.size() && p1_coefs.size() == 1)
        {
            std::vector<T> poly = { p1_coefs[0] * p2_coefs[0] };
            return poly;
        }

        size_t n = p1_coefs.size();

        std::vector<T> p1_left = GetLeftSide(p1_coefs);
        std::vector<T> p1_right = GetRightSide(p1_coefs);
        std::vector<T> p2_left = GetLeftSide(p2_coefs);
        std::vector<T> p2_right = GetRightSide(p2_coefs);

        std::vector<T> c3 = AddVectors(p1_left, p1_right);
        std::vector<T> c4 = AddVectors(p2_left, p2_right);
        
        std::vector<T> c1, c2, c5;
        if (use_threads)
        {
            use_threads = false;

            std::vector<std::future<std::vector<T>>> results;
            results.push_back
            (
                std::async
                (
                    [this, &use_threads](std::vector<T> p1, std::vector<T> p2) // c1
                    {
                        return MultiplyKaratsubaVectors(p1, p2, use_threads);
                    },
                    p1_left,
                    p2_left
                )
            );

            results.push_back
            (
                std::async
                (
                    [this, &use_threads](std::vector<T> p1, std::vector<T> p2) // c2
                    {
                        return MultiplyKaratsubaVectors(p1, p2, use_threads);
                    },
                    p1_right,
                    p2_right
                )
            );

            results.push_back
            (
                std::async
                (
                    [this, &use_threads](std::vector<T> p1, std::vector<T> p2) // c5
                    {
                        return MultiplyKaratsubaVectors(p1, p2, use_threads);
                    },
                    c3,
                    c4
                )
            );

            c1 = results[0].get();
            c2 = results[1].get();
            c5 = results[2].get();
        }
        else
        {
            c1 = MultiplyKaratsubaVectors(p1_left, p2_left, false);
            c2 = MultiplyKaratsubaVectors(p1_right, p2_right, false);
            c5 = MultiplyKaratsubaVectors(c3, c4, false);
        }
        std::vector<T> c6 = SubtractVectors(SubtractVectors(c5, c1), c2);
        FillBeginUntilSizeEqualTo(c6, c6.size() + n / 2);
        FillBeginUntilSizeEqualTo(c2, c2.size() + n);

        std::vector<T> tmp = AddVectors(c1, c6);
        std::vector<T> tmp2 = AddVectors(tmp, c2);
        return tmp2;
    }

    template<typename T>
    inline void Polynomial<T>::FillEndUntilSizePowOfTwo(std::vector<T>& vec)
    {
        while (!utils::IsPowerOfTwo(vec.size()))
            vec.push_back(_fill_val);
    }

    template<typename T>
    inline void Polynomial<T>::FillUntilSizeEqualTo(size_t size)
    {
        while (_coefs.size() < size)
            _coefs.push_back(_fill_val);
    }

    template<typename T>
    inline void Polynomial<T>::FillBeginUntilSizeEqualTo(std::vector<T>& vec, size_t size)
    {
        while (vec.size() < size)
            vec.insert(vec.begin(), _fill_val);
    }

    template<typename T>
    inline void Polynomial<T>::FillEndUntilSizeEqualTo(std::vector<T>& vec, size_t size)
    {
        while (vec.size() < size)
            vec.push_back(_fill_val);
    }

    template<typename T>
    inline std::vector<T> Polynomial<T>::GetLeftSide(std::vector<T>& vec)
    {
        std::vector<T> res;
        size_t i = 0;
        while (res.size() < vec.size() / 2)
            res.push_back(vec[i++]);

        return res;
    }

    template<typename T>
    inline std::vector<T> Polynomial<T>::GetRightSide(std::vector<T>& vec)
    {
        std::vector<T> res;
        size_t i = vec.size() / 2;
        while (res.size() < vec.size() / 2)
            res.push_back(vec[i++]);

        return res;
    }

    template<typename T>
    inline std::vector<T> Polynomial<T>::AddVectors(std::vector<T> v1, std::vector<T> v2)
    {
        std::vector<T> res;
        res.reserve((std::max)(v1.size(), v2.size()));

        for (size_t i = 0; i < res.capacity(); ++i)
        {
            T& a = v1.size() <= i ? _fill_val : v1[i];
            T& b = v2.size() <= i ? _fill_val : v2[i];
            res.push_back(a + b);
        }

        return res;
    }

    template<typename T>
    inline std::vector<T> Polynomial<T>::SubtractVectors(std::vector<T> v1, std::vector<T> v2)
    {
        std::vector<T> res;
        res.reserve((std::max)(v1.size(), v2.size()));

        for (size_t i = 0; i < res.capacity(); ++i)
        {
            T& a = v1.size() <= i ? _fill_val : v1[i];
            T& b = v2.size() <= i ? _fill_val : v2[i];
            res.push_back(a - b);
        }

        return res;
    }

    template<typename T>
    inline Polynomial<T>::Polynomial(size_t degree)
        : _coefs(degree, 0)
    {}

    template<typename T>
    inline hrv::Polynomial<T>& Polynomial<T>::SetFillValue(const T & val)
    {
        _fill_val = val;

        return *this;
    }

    template<typename T>
    inline hrv::Polynomial<T>& Polynomial<T>::SetCoefAt(const size_t & index, const T & val)
    {
        _coefs[index] = val;

        return *this;
    }

    template<typename T>
    inline T & Polynomial<T>::GetCoefAt(const size_t & index)
    {
        return _coefs[index];
    }

    template<typename T>
    Polynomial<T> operator+(Polynomial<T> lhs, const Polynomial<T>& rhs)
    {
        size_t max_degree = (std::max)(lhs._coefs.size(), rhs._coefs.size());
        Polynomial<T> res(max_degree);

        for (size_t i = 0; i < max_degree; ++i)
            if (i < lhs._coefs.size())
                if (i < rhs._coefs.size())
                    res._coefs[i] = lhs._coefs[i] + rhs._coefs[i];
                else
                    res._coefs[i] = lhs._coefs[i];
            else
                res._coefs[i] = rhs._coefs[i];

        return res;
    }

    template<typename T>
    hrv::Polynomial<T> operator-(Polynomial<T> lhs, const Polynomial<T>& rhs)
    {
        size_t max_degree = (std::max)(lhs._coefs.size(), rhs._coefs.size());
        Polynomial<T> res(max_degree);

        for (size_t i = 0; i < max_degree; ++i)
            if (i < lhs._coefs.size())
                if (i < rhs._coefs.size())
                    res._coefs[i] = lhs._coefs[i] - rhs._coefs[i];
                else
                    res._coefs[i] = lhs._coefs[i];
            else
                res._coefs[i] = -rhs._coefs[i];

        return res;
    }

    template<typename U>
    hrv::Polynomial<U> operator*(const int & val, const Polynomial<U>& rhs)
    {
        return rhs.operator*(val);
    }

    template<typename T>
    inline hrv::Polynomial<T> Polynomial<T>::operator*(const T & val) const
    {
        Polynomial<T> res(_coefs.size());
        for (size_t i = 0; i < _coefs.size(); ++i)
            res._coefs[i] = val * _coefs[i];

        return res;
    }

    template<typename T>
    inline hrv::Polynomial<T> Polynomial<T>::MultiplyRegularSingleThreaded(const Polynomial<T>& ot)
    {
        Polynomial<T> res(_coefs.size() + ot._coefs.size() - 1);

        for (size_t i = 0; i < _coefs.size(); ++i)
            for (size_t j = 0; j < ot._coefs.size(); ++j)
                res._coefs[i + j] += _coefs[i] * ot._coefs[j];

        return res;
    }

    template<typename T>
    inline hrv::Polynomial<T> Polynomial<T>::MultiplyRegularMultiThreaded(const Polynomial<T>& ot, const size_t& thread_count)
    {
        size_t total = _coefs.size() + ot._coefs.size() - 1;

        Polynomial<T> res(total);

        size_t from = 0,
            to = 0,
            step = (total) / thread_count,
            left = (total) % thread_count;

        std::vector<std::future<void>> results;
        results.reserve(total);
        for (size_t i = 0; i < thread_count; ++i)
        {
            from = to;
            to += step;
            if (left)
            {
                --left;
                ++to;
            }

            results.push_back
            (
                std::async
                (
                    [&res, this, &ot](size_t from, size_t to)
                    {
                        std::vector<int> ot_coefs = ot.GetCoefs();
                        LOG(INFO) << "Processing polynomial slice from " << from << " to " << to;
                        for (size_t crt = from; crt < to; ++crt)
                        {
                            size_t begin = crt < ot_coefs.size() ? 0 : crt - ot_coefs.size() + 1;
                            size_t end = (std::min)(crt + 1, _coefs.size());
                            for (size_t i = begin; i < end; ++i)
                                res.SetCoefAt(crt, res.GetCoefAt(crt) + _coefs[i] * ot_coefs[crt - i]);
                        }
                    },
                    from,
                    to
                )
            );
        }

        for (auto&& result : results)
            result.get();

        return res;
    }

    template<typename T>
    inline hrv::Polynomial<T> Polynomial<T>::MultiplyKaratsubaSingleThreaded(Polynomial<T>& ot)
    {
        std::vector<T> res = MultiplyKaratsubaVectors(_coefs, ot._coefs, false);
        utils::TrimFromEnd(res, _fill_val);
        Polynomial<T> c;
        c.SetFillValue(_fill_val);
        c.SetCoefs(res.begin(), res.end());

        return c;
    }

    template<typename T>
    inline hrv::Polynomial<T> Polynomial<T>::MultiplyKaratsubaMultiThreaded(Polynomial<T>& ot)
    {
        std::vector<T> res = MultiplyKaratsubaVectors(_coefs, ot._coefs, true);
        utils::TrimFromEnd(res, _fill_val);
        Polynomial<T> c;
        c.SetFillValue(_fill_val);
        c.SetCoefs(res.begin(), res.end());

        return c;
    }

    template<typename T>
    inline Polynomial<T>::~Polynomial()
    {
        _coefs.clear();
    }

    template<typename T>
    template<class InputIterator>
    inline hrv::Polynomial<T>& Polynomial<T>::SetCoefs(InputIterator first, InputIterator last)
    {
        _coefs.reserve(last - first);

        for (; first != last; ++first)
            _coefs.push_back(*first);

        return *this;
    }

    template<typename T>
    size_t hrv::GetDegree(const Polynomial<T>& poly)
    {
        if (poly._coefs.size() == 0)
            return 0;

        size_t non_default_vals = 0;
        const std::vector<T>& coefs = poly._coefs;
        for (size_t i = 0; i < coefs.size(); ++i)
            if (coefs[i] != poly._fill_val)
                ++non_default_vals;

        return non_default_vals;
    }
}
