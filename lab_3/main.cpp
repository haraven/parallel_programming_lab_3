#include <boost\timer\timer.hpp>
#define ELPP_THREAD_SAFE
#define ELPP_STL_LOGGING
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP

#include "polynomial.hpp"
#include <algorithm>

enum RetCode
{
    SUCCESS = 0,
    FAILURE = -1
};

int main()
{
    el::Loggers::configureFromGlobal("logging.conf");

    hrv::Polynomial<int> poly1, poly2;
    std::vector<int> p1 = { 1, 2, 3, 4, 5, 6, 7 };
    std::vector<int> p2 = { 1, 2, 3, 4, 5, 6, 7 };
    poly1.SetFillValue(0);
    poly2.SetFillValue(0);
    poly1.SetCoefs(p1.begin(), p1.end());
    poly2.SetCoefs(p2.begin(), p2.end());
    std::vector<int> res = poly1.MultiplyRegularMultiThreaded(poly2, 2).GetCoefs();
    
    LOG(INFO) << res << std::endl;

    return SUCCESS;
}