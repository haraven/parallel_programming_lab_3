#pragma once
#include <vector>

namespace hrv
{
    namespace utils
    {
        bool IsPowerOfTwo(unsigned long long x);

        template<typename T>
        void TrimFromEnd(std::vector<T>& vec, const T& val)
        {
            do
            {
                vec.erase(vec.begin() + vec.size() - 1);
            } while (vec[vec.size() - 1] == val);
        }
    }
}